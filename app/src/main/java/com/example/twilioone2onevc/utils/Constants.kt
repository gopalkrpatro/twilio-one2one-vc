package com.example.twilioone2onevc.utils

object Constants {
    const val BASE_URL = "https://vri.totallanguage.com"
    const val GET_ACCESS_TOKEN = "/apitoken"
}