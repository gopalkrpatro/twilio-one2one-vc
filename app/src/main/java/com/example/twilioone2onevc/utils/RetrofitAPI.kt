package com.example.twilioone2onevc.utils

import com.example.twilioone2onevc.models.AccessTokenResponseModel
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitAPI {
    @GET(Constants.GET_ACCESS_TOKEN)
    fun getAccessToken(): Call<AccessTokenResponseModel>
}