package com.example.twilioone2onevc.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build


class AndroidUtils {

    companion object {

        fun isNetworkOnLine(context: Context): Boolean {
            try {
                val cm =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
                    if (capabilities != null) {
                        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                            return true
                        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                            return true
                        }
                    }
                } else {
                    val activeNetwork = cm.activeNetworkInfo
                    if (activeNetwork != null) {
                        if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                            return true
                        } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                            return true
                        }
                    }
                }
            } catch (e: Exception) {
                return false
            }
            return false
        }
    }
}