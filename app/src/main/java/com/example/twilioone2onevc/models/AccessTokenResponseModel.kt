package com.example.twilioone2onevc.models

import com.google.gson.annotations.SerializedName

data class AccessTokenResponseModel(
    @SerializedName("identity")
    var identity: String,
    @SerializedName("sname")
    var sname: String,
    @SerializedName("token")
    var token: String
)
